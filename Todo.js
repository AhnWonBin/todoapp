import React, {Component} from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
} from "react-native";
import PropTypes from "prop-types";

/**
 * stateless component 가 아닌 stateful component 인 이유
 * 화면에서 수정을 누르면 state를 수정모드로 변경해야 하기 때문
 * 
 * ToDo는 2개의 state로 구분
 * 1. 수정할 때
 * 2. 수정하지 않을 때
 */

const { width, height } = Dimensions.get("window");

export default class ToDo extends Component {
    constructor(props) {
        super(props);
        this.state = { isEditing: false, toDoValue: props.text };
    }

    static propTypes = {
        text: PropTypes.string.isRequired,
        isCompleted: PropTypes.bool.isRequired,
        deleteToDo: PropTypes.func.isRequired,
        completeToDo: PropTypes.func.isRequired,
        uncompleteToDo: PropTypes.func.isRequired,
        updateToDo: PropTypes.func.isRequired,
        id: PropTypes.string.isRequired
    };

    state = {
        isEditing: false,
        toDoValue: "",
    };

    render() {
        const { isEditing, toDoValue } = this.state;
        const { text, id, deleteToDo, isCompleted } = this.props;

        return(
            <View style={styles.container}>
                <View style={styles.column}>
                    <TouchableOpacity onPress={this._toggleComplete}>
                        <View 
                            style={[
                                styles.circle, 
                                isCompleted ? styles.completedCircle : styles.uncompletedCircle
                                ]}
                        />
                    </TouchableOpacity>
                    {isEditing ? (
                        <TextInput 
                            style={[
                                    styles.text,
                                    styles.input,
                                    isCompleted ? styles.completedText : styles.uncompletedText
                                ]} 
                            value={toDoValue}
                            multiline={true}
                            onChangeText={this._controlInput}
                            returnKeyType={"done"}
                            onBlur={this._finishEditing} // 다른 레이아웃 클릭 시 발생하는 이벤트
                        />
                    ) : (
                        <Text style={[
                            styles.text,
                            isCompleted ? styles.completedText : styles.uncompletedText
                            ]}>
                            {text}
                        </Text>
                    )}
                </View>
                    { isEditing ? (
                        <View style={styles.actions}>
                            <TouchableOpacity onPressOut={this._finishEditing}>
                                <View style={styles.actionContainer}>
                                    <Text style={styles.actionText}>완료</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <View style={styles.actions}>
                            <TouchableOpacity onPressOut={this._startEditing}>
                                <View style={styles.actionContainer}>
                                    <Text style={styles.actionText}>수정</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPressOut={(event) => {
                                event.stopPropagation;
                                deleteToDo(id);
                                }}>
                                <View style={styles.actionContainer}>
                                    <Text style={styles.actionText}>삭제</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ) }
            </View>
        );
    }

    _toggleComplete = (event) => {
        event.stopPropagation();
        const { isCompleted, uncompleteToDo, completeToDo, id } = this.props;

        if(isCompleted){
            uncompleteToDo(id);
        }else{
            completeToDo(id);
        }
    };
    
    _startEditing = (event) => {
        event.stopPropagation();
        this.setState({
            isEditing: true
        });
    };

    _finishEditing = (event) => {
        event.stopPropagation();
        const { toDoValue } = this.state;
        const { id, updateToDo } = this.props;
        updateToDo(id, toDoValue);
        this.setState({
            isEditing: false
        });
    };

    _controlInput = (text) => {
        this.setState({
            toDoValue: text
        });
    };

    
}

const styles = StyleSheet.create({
    container: {
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center",
        width: width - 50,
        borderBottomColor: "#bbb",
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    column: {
        flexDirection: "row",
        alignItems: "center",
        // justifyContent: "space-between",
        width: width / 2,
    },
    circle: {
        width: 30,
        height: 30,
        // borderRadius는 항상 width, height의 절반이어야 한다
        borderRadius: 15,
        // backgroundColor: "red",
        borderColor: "red",
        borderWidth: 3,
        marginRight: 20,
    },
    completedCircle: {
        borderColor: "#bbb"
    },
    uncompletedCircle: {
        borderColor: "#F23657"
    },
    text: {
        fontSize: 20,
        fontWeight: "600",
        // 마진 상하단
        marginVertical: 20,
    },
    completedText: {
        color: "#bbb",
        textDecorationLine: "line-through"
    },
    uncompletedText: {
        color: "#353839"
    },
    actions: {
        flexDirection: "row"
    },
    actionContainer: {
        marginVertical: 10,
        marginHorizontal: 10
    },
    input: {
        width: width / 2,
        marginVertical: 15,
        paddingBottom: 5,
    },
    actionText: {
        
    },
});
