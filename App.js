import React, {Component} from 'react';
import {
  Platform,
  StyleSheet, 
  Text, 
  View, 
  StatusBar, 
  TextInput, 
  Dimensions,
  ScrollView,
  AsyncStorage,
} from 'react-native';

import uuidv1 from "uuid/v1";
import ToDo from "./Todo";

/*
let data = JSON.stringify(this.state);
// 데이터 저장
// AsyncStorage : String 저장용이므로 Object 저장 시 변환 필요
// key : value 형식이며 String 이기만 하면 된다
AsyncStorage.setItem("변수명", data, () => {

});
// 데이터 불러오기
AsyncStorage.getItem("변수명", (err, value) => {
  if(err == null){
    let json = JSON.parse(value);
  }
});

AsyncStorage.getItem("변수명").then((key) => {
  
}).catch((err) => {
  console.log(err);
});

*/

/**
 * fixed
 * @date 2018.12.14
 * 1. 스크롤 뷰 동작 문제
 * 2. 데이터 수정 시 출력 순서 변경되는 문제
 */

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const { height, width } = Dimensions.get("window");


export default class App extends Component {
  state = {
    newToDo: "",
    loadedToDos: false,
    toDos: {}
  };

  componentDidMount = () => {
    this._loadToDos();
  }

  render() {
    const { newToDo, loadedToDos, toDos } = this.state;
    console.log(toDos);

    if(!loadedToDos) {
      // return <AppLoading />
    }

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <Text style={styles.title}>testestestest</Text>
        <View style={styles.card}>
          <TextInput 
            style={styles.input}  
            placeholder={"New To Do"} 
            placeholderTextColor={"#999"}
            value={newToDo} 
            onChangeText={this._controlNewToDo}
            returnKeyType={"done"}
            autoCorrect={false}
            onSubmitEditing={this._addToDo}
          />
          <ScrollView contentContainerStyle={styles.toDos}>
            {/* <ToDo text="Hello To Do App." /> */}
            {Object.values(toDos)
              .reverse()
              .map(toDo => (
              <ToDo key={toDo.id}
              deleteToDo={this._deleteToDo}
              uncompleteToDo={this._uncompleteToDo}
              completeToDo={this._completeToDo}
              updateToDo={this._updateToDo}
              {...toDo} 
              />
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }

  _controlNewToDo = text => {
    this.setState({
      newToDo: text
    });
  };

  // 화면 로딩이 끝나면 데이터 세팅
  _loadToDos = async() => {
    try{
      // getItem 실행이 끝날때까지 기다림 async await
      const toDos = await AsyncStorage.getItem("toDos");
      const parsedToDos = JSON.parse(toDos);
      console.log("load toDos : " + toDos);

      this.setState({
        loadedToDos: true,
        toDos: parsedToDos
      });
    } catch(err) {
      console.log("err : " + err);
    }
  };

  // todo를 state에서 가져온다
  // 그리고 체크 후 공백이 아니라면 서브밋 할 수있도록
  _addToDo = () => {
    const { newToDo } = this.state;
    if(newToDo !== ""){
      this.setState(prevState => {
        const ID = uuidv1();
        const newToDoObject = {
          [ID]: {
            id: ID,
            isCompleted: false,
            text: newToDo,
            createdAt: Date.now()
          }
        };

        const newState = {
          ...prevState,
          newToDo: "",
          toDos: {
            ...prevState.toDos,
            ...newToDoObject
          }
        };
        this._saveToDo(newState.toDos);
        return { ...newState };
      });
    }
  };

  _deleteToDo = (id) => {
    this.setState(prevState => {
        const toDos = prevState.toDos;
        delete toDos[id];
        const newState = {
            ...prevState,
            ...toDos
        };
        this._saveToDo(newState.toDos);
        return { ...newState };
    });
  };

  _uncompleteToDo = (id) => {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        toDos: {
          ...prevState.toDos,
          [id]: {
            ...prevState.toDos[id],
            isCompleted: false
          }
        }
      };
      this._saveToDo(newState.toDos);
      return { ...newState };
    });
  };


  _completeToDo = (id) => {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        toDos: {
          ...prevState.toDos,
          [id]: {
            ...prevState.toDos[id],
            isCompleted: true
          }
        }
      };
      this._saveToDo(newState.toDos);
      return { ...newState };
    });
  };

  _updateToDo = (id, text) => {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        toDos: {
          ...prevState.toDos,
          [id]: {
            ...prevState.toDos[id],
            text: text
          }
        }
      };
      // 업데이트 저장
      this._saveToDo(newState.toDos);
      return { ...newState };
    });
  };

  _saveToDo = (newToDos) => {
    // Object -> String 변환
    // console.log(JSON.stringify(newToDos));
    const saveToDo = AsyncStorage.setItem("toDos", JSON.stringify(newToDos));
  };

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F23657',
  },
  title: {
    color: "white",
    fontSize: 30,
    fontWeight: "200",
    marginTop: 40,
    marginBottom: 30,
  },
  card: {
    flex: 1,
    backgroundColor: "white",
    width: width - 25,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    ...Platform.select({
      ios: {
        shadowColor: "rgb(50, 50, 50)",
        shadowOpacity: 0.5,
        shadowRadius: 5,
        shadowOffset: {
          height: -1,
          width: 0
        }
      },
      android: {
        elevation: 3
      }
    }),
  },
  input: {
    padding: 20,
    borderBottomColor: "#bbb",
    borderBottomWidth: 1,
    fontSize: 25,
  },
  toDos: {
    alignItems: "center",
  }
});
