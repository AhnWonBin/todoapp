#TodoApp

TodoApp made React Native

vscode - debug plugin
https://github.com/Microsoft/vscode-react-native

ctrl + shift + x => market => search and react native tools install => reload
=> launch.json in .vscode folder => Add Configuration button click => add React Native : Debug Android
=> Menu - View - debug console click => finish


2018.12.14
# fixed
- ScrollView 동작 문제
- To Do 리스트 update 시 순서 변경 문제

# work
- 로딩 화면 작업 : https://medium.com/handlebar-labs/how-to-add-a-splash-screen-to-a-react-native-app-ios-and-android-30a3cec835ae
./android/app/src/main/res 폴더에
drawable 폴더 생성 background_splash.xml 추가
values 폴더에 colors.xml 추가

- create : background_splash.xml
- create : colors.xml

- modify : styles.xml
line 6, line 9-13

- modify : AndroidManifest.xml
android:theme="@style/SplashTheme"
android:exported="true"